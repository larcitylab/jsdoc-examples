/**
* @description A jsdoc example of a module and some notes about what it does
* @module Helpers
*/
module.exports = {
    /**
     * @description Takes a string, and returns it capitalized
     * @param {String} someString 
     */
    allCaps
}

function allCaps(someString) {
    return String(someString).toUpperCase()
}

