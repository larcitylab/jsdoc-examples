'use strict'

/**
 * @description Defines a Tool class
 */
class Tool {
    constructor(name, desc) {
        this.name = name
        this.description = desc
    }

    set name(newName) {
        this.name = newName
    }

    get name() {
        return this.name
    }

    get description() {
        return this.description
    }

}

module.exports = Tool