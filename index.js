const express = require('express')
const app = express()
app.use(express.static('docs'))

require('colors')

let { PORT } = process.env
if (!PORT) PORT = 5000

app.listen(PORT, () => console.log(`Documentation server listening @${PORT}`.bgWhite.black))