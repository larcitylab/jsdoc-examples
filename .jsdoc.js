'use strict'

module.exports = {
    plugins: [
        'plugins/markdown'
    ],
    //recurseDepth: 10,
    source: {
        includePattern: '.+\\.js(doc|x)?$',
        excludePattern: '(^|\\/|\\\\)_',
        exclude: [
            './node_modules',
            'docs'
        ]
    },
    sourceType: 'module',
    tags: {
        allowUnknownTags: true,
        dictionaries: ['jsdoc']
    },
    templates: {
        cleverLinks: false,
        monospaceLinks: false,
        referenceTitle: "JSDoc Examples",
        useLongnameInNav: false,
        showInheritedInNav: true,
        disableSort: false
    },
    opts: {
        destination: './public/',
        encoding: 'utf8',
        private: false,
        recurse: true,
        template: './node_modules/jsdoc-template',
        //template: './node_modules/minami'
    }
}