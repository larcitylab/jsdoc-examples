const ghpages = require('gh-pages')
const should = require('should')

describe('Publishes to github', () => {
    it('Publishes to github', done => {
        ghpages.publish('docs', (err) => {
            should.not.exist(err)
            done()
        })
    })
})

